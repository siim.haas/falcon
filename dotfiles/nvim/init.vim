call plug#begin('~/.config/nvim/plugged')
Plug 'preservim/nerdcommenter'
Plug 'morhetz/gruvbox'
Plug 'ycm-core/YouCompleteMe'
Plug 'tpope/vim-surround'
Plug 'preservim/nerdtree'
Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }
Plug 'hashivim/vim-vagrant'
call plug#end()

syntax enable
set backspace=2 "make backspace work like most other programs
set encoding=utf-8
set expandtab
set fileencodings=utf-8
set incsearch
set nowrap
set shiftwidth=4
set smartindent
set tabstop=4 softtabstop=4
set mouse=a
set hidden

"Search down into subfolders"
"Provides tab-completion for all file-related tasks"
set path+=**

"Display all matching files when we tab complete"
set wildmenu

"Welcome to current millenium"
set nocompatible

"Create the 'tags' file"
" - Use ^] to jump to tag under cursor"
" - Use g^] for ambiguous tags"
" - Use ^t to jump back up the tag stack"
command! MakeTags !ctags -R .

filetype plugin on

set relativenumber
set number

" zz - move cursor in middle of screen
nnoremap k kzz
nnoremap j jzz

" allow saving of files as sudo when I forgot to start vim using sudo
cmap w!! w !sudo tee > /dev/null %
set background=dark
colorscheme solarized


" Toggle Vexplore with Ctrl-E
" function! ToggleVExplorer()
"   if exists("t:expl_buf_num")
"       let expl_win_num = bufwinnr(t:expl_buf_num)
"       if expl_win_num != -1
"           let cur_win_nr = winnr()
"           exec expl_win_num . 'wincmd w'
"           close
"           exec cur_win_nr . 'wincmd w'
"           unlet t:expl_buf_num
"       else
"           unlet t:expl_buf_num
"       endif
"   else
"       exec '1wincmd w'
"       Vexplore
"       let t:expl_buf_num = bufnr("%")
"   endif
" endfunction
let g:netrw_banner = 0
let g:netrw_winsize = 25
map <silent> <F2> :Lexplore<CR>
let mapleader = "\<Space>"
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1
