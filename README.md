blacklisted bluetooth and webcam module
undo:
delete /etc/modprobe/bootblacklist.conf
remove path above from 'FILES' in /etc/mkinitcpio.conf
generate kernel img with 'mkinitcpio -p linux'


boot broke with kernel 5.3.7-2
LOG: fb0: switching to inteldrmfb from efi vga
this is related to x200 and core/libreboot
Fix is:
1) wait for next kernel release
2) add 'blacklist gsmi' to /etc/modprobe.d/blacklist.conf
Currently I went with second option.
